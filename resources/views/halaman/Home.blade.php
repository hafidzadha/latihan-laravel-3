@extends('layout.master')

@section('judul')
    Media Online
@endsection

@section('content')
<h3>Sosial Media Developer</h3>

<p>Belajar dan berbagi agar hidup menjadi lebih baik</p>
<b>Benefit Join di Media Online</b>

<ul>
    <li>Mendapatkan motivasi dari sesama para Developer</li>
    <li>Sharing knowlenge</li>
    <li>Dibuat oleh calon web developer terbaik</li>
</ul>

<b>Cara Bergabung ke Media Online</b>

<ol>
    <li>Mengunjungi Website ini</li>
    <li>Mendaftarkan di <a href="/register" target_self>Form Sign Up</a></li>
    <li>Selesai</li>
</ol>
@endsection
